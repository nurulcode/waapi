
const auth = (req, res, next) => {
    if (req.user && req.user.isauth) {
        next();
    } else {
        res.status(401);
        throw new Error("Not authorized as an admin");
    }
};

export { auth };
